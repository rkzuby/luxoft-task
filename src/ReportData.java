public class ReportData {

    private String team;
    private long totalEffort;
    private long effort;

    public ReportData(){

    }

    @Override
    public String toString() {
        return "ReportData{" +
                "team='" + team + '\'' +
                ", totalEffort=" + totalEffort +
                ", remaining=" + effort +
                '}';
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public long getTotalEffort() {
        return totalEffort;
    }

    public void setTotalEffort(long totalEffort) {
        this.totalEffort += totalEffort;
    }

    public long getRemainingEffort() {
        return effort;
    }

    public void setEffort(long effort) {
        this.effort += effort;
    }
}
