import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static ReportData reportData = new ReportData();

    private static Set<String> teams = Stream.of("India", "Kiev", "London", "NY", "UI")
                                            .collect(Collectors.toSet());

    public static void main(String[] args) {


        try {
            File inputF = new File("subtasks-0504.csv");
            InputStream inputFS = new FileInputStream(inputF);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            // skip the header of the csv
            br.lines().skip(1).forEach(Main::mapRecordToItem);
            br.close();
        } catch (IOException e) {
            System.out.println("Oops!");
        }

        System.out.println(reportData.toString());
    }

    public static void mapRecordToItem(String line) {

        String[] record = line.split(",");
        reportData.setTeam(record[8]);
        reportData.setTotalEffort(Long.parseLong(record[6]));
        if (!record[4].contains("Closed")) {
            reportData.setEffort(Long.parseLong(record[6]));
        }
    }
}
